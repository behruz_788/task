<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEmailToken extends Model
{
    protected $table = "user_email_token";

    protected $primaryKey="uet_id";

    protected $guarded = [];

}
