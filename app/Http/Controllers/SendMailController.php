<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendVerificationMail;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    public function sendMail() {
        $user = User::find(6);
        $data['token'] = Str::random(10);
        // dd($user);
        Mail::to($user)->send(new SendVerificationMail('salom'));

        return redirect()->back();
    }

    public function getVerification($token) {

        dd($token);
    }
}
