<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Image;
use File;
use App\Profile;

class ProfileController extends Controller
{
    public function getUserProfile() {
        $user = User::with('profile')->findOrFail(auth()->user()->id);

        return view('pages.user-edit',compact('user'));
    }

    public function editUserCreds(Request $request) {
        // dd($request->all());
        $user = $request->all();
        $u = User::findOrFail($request->input('userId'));

        if(isset($user['userName'])) {
            $u->name = $user['userName'];
        }
        else if(isset($user['userEmail'])) {
            $u->email = $user['userEmail'];
            $u->email_verified_at = null;
        }
        else if(isset($user['newPassword'])) {
            $u->password = bcrypt($user['newPassword']);
        }

        $u->save();

        return redirect()->back();
    }

    public function checkPassword(Request $request) {
        // return $request->all();
        $user = User::findOrFail($request->input('userId'));

        if(Hash::check($request->input('pass'),$user->password)) {
            return response()->json(['message' => 'success']);
        } 
        return response()->json(['message' => 'fail']);
    }

    public function addUserAvatar(Request $request) {
        // dd($request->userAvatar);
        $user = User::findOrFail($request->input('userId')); 
        $data =[];
        $data['avatar'] = $this->userAvatar($request->userAvatar,$user,'original');
        $data['avatar_medium'] = $this->userAvatar($request->userAvatar,$user,'medium',300,300);
        $data['avatar_thumb'] = $this->userAvatar($request->userAvatar,$user,'thumb',150,150);
        $data['user_id'] = $user->id;
        

        $profile = Profile::create($data);

        return redirect()->back();
    }

    private function userAvatar($image,$user,$name,$width=null,$height=null) {

        $extension = $image->getClientOriginalExtension();
        $img = Image::make($image);               
        if($width && $height) {
            $img->resize($width,$height);
        }
        

        $file_name = $user->name.'_'.$name.'.'.$extension;


        $path = 'uploads/avatars/'.$user->name;
        if(!file_exists(public_path().'/'.$path)) {
            File::makeDirectory($path,0755,true);
        }
        $img->save(public_path($path.'/'.$file_name));
        $file_path = '/'.$path.'/'.$file_name;       

        return $file_path;

    }
}
