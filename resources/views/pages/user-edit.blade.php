@extends('layouts.app')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @if($user->profile)
                <img src="{{$user->profile->avatar}}" class="img-fluid" alt="avatar">
                
                @else
                  <img src="{{asset('images/avatar.png')}}" class="img-fluid" alt="avatar">
                  
                @endif
                <div class="mt-4">
                <form action="/add-user-avatar" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="userId" value="{{$user->id}}">
                    <input type="file" name="userAvatar" required>
                    <div class="row justify-content-end">
                        <button class="btn btn-primary">Upload</button>
                    </div>
                </form>
                </div>
            </div>
            <div class="col-md-8">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                <div class="row">
                   <div class="col-md-10"> 
                     <div id="name"><strong>Name: </strong>{{$user->name}}</div>
                     <div class="d-none" id="nameEditForm">
                     <form action="/edit-user-creds" method="post">
                       {{csrf_field()}}
                        <input type="hidden" name="userId" value="{{$user->id}}">
                        <input type="text" class="form-control" name="userName" value="{{$user->name}}">
                        <div class="justify-content-end row mr-1 mt-1">
                        <button class="btn btn-outline-success btn-sm mr-1">Edit</button>
                        <button class="btn btn-outline-danger btn-sm " type="button" onClick="nameEditCancel()">cancel</button>
                        </div>
                     </form>
                     </div>
                   </div>
                   <div class="col-md-2">
                    <button type="button" class="btn btn-primary justify-content-end" id="nameEditBtn" onClick = "nameEditBtn(this)">edit</button>
                   </div>
                   </div>
                </li>
                <li class="list-group-item">
                  <div class="row">  
                    <div class="col-md-10">
                    <div id="email"><strong>Email: </strong>{{$user->email}}</div>
                    <div class="d-none" id="emailEditForm">
                    <form action="/edit-user-creds" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="userId" value="{{$user->id}}">
                        <input type="text" class="form-control" name="userEmail" value="{{$user->email}}">
                        <div class="justify-content-end row mr-1 mt-1">
                        <button class="btn btn-success btn-sm mr-1">Edit</button>
                        <button class="btn btn-outline-danger btn-sm" type="button" onClick = "emailEditCancel()">cancel</button>
                        </div>
                     </form>
                     </div>
                    </div>
                    <div class="col-md-2">                    
                     <button class="btn btn-primary  justify-content-end" id="emailEditBtn" onClick="emailEditBtn(this)" >edit</button>
                                  
                   
                    </div>
                    </div>
                    </li>
                </ul>
                <div class="mt-2 col-md-12">
                    <a class="text-primary justify-content-end" id="changePassword" style="cursor:pointer">Change Password</a>
                    <div class="d-none" id="checkUserPass">
                    <label for="oldPassword">Your Old Password</label>
                     <input type="password" name="checkOldPass" class="form-control" id="checkPass">
                     <div class="justify-content-end row mr-2 mt-1">
                     <button class="btn btn-warning btn-sm " id="cancelOldPassForm">cancel</button>
                     </div>
                     <div class="d-none mt-2" id="newPass">
                       <form action="/edit-user-creds" method="post">
                       {{csrf_field()}}
                        <input type="hidden" name="userId" value="{{$user->id}}">
                        <label for="newpassword">Enter Your New Password</label>
                        <input type="password" name = "newPassword" class="form-control" >
                        <div class="justify-content-end row mt-1 mr-1">
                        <button class="btn btn-success btn-sm  justify-content-end">Edit</button>
                        </div>
                       </form>
                     </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
    function nameEditBtn(e) {
        console.log(e)
        $(e).addClass('d-none')
        $('#name').addClass('d-none')
        $('#nameEditForm').removeClass('d-none')
    }
    
    function nameEditCancel() {
          $('#nameEditBtn').removeClass('d-none')
        $('#name').removeClass('d-none')
        $('#nameEditForm').addClass('d-none')
    }

    function emailEditBtn(e) {
        $(e).addClass('d-none')
        $('#email').addClass('d-none')
        $('#emailEditForm').removeClass('d-none')
    }

    function emailEditCancel() {
          $('#emailEditBtn').removeClass('d-none')
        $('#email').removeClass('d-none')
        $('#emailEditForm').addClass('d-none')
    }

    
    $(document).on('click','#changePassword', function() {
        $('#checkUserPass').removeClass('d-none');
    });
    $(document).on('click','#cancelOldPassForm', function() {
        $('#checkUserPass').addClass('d-none');
    });
    var userId = {{$user->id}}
       
    $(document).on('input','#checkPass', function() {
        var pass = $('#checkPass').val();
       
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:'/check-password',
            method:'post',
            data: {"pass":pass,"userId":userId},
            success:function(data) {                
                if(data.message == 'success') {                    
                    $('#checkPass').removeClass('is-invalid').addClass('is-valid');
                    $('#newPass').removeClass('d-none');
                }
                else{
                    $('#checkPass').removeClass('is-valid').addClass('is-invalid')
                 
                }
            }
        })
    })
      
    </script>
@endsection