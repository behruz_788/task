<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth','verified']],function() {
    Route::get('/', function () {
        return redirect('/home');
    });
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/user-profile','ProfileController@getUserProfile')->name('userprofile');
    Route::post('/edit-user-creds','ProfileController@editUserCreds');
    Route::post('/check-password','ProfileController@checkPassword');
    Route::post('/add-user-avatar','ProfileController@addUserAvatar');

});


Auth::routes(['verify' => true]);

